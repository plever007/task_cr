<?php

namespace app\models;

use DateTime;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $personal_code
 * @property int $phone
 * @property bool|null $active
 * @property bool|null $dead
 * @property string|null $lang
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * @var
     */
    public $username;
    /**
     * @var
     */
    public $password;
    /**
     * @var
     */
    public $authKey;
    /**
     * @var
     */
    public $accessToken;

    public const STATUS_ACTIVE = true;

    /**
     * @var array
     */
    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'personal_code', 'phone'], 'required'],
            [['first_name', 'last_name', 'email', 'lang'], 'string'],
            [['personal_code', 'phone'], 'default', 'value' => null],
            [['personal_code', 'phone'], 'integer'],
            [['active', 'dead'], 'boolean'],
            [['email'], 'email'],
            ['personal_code', 'personalCodeLength'],
            ['personal_code', 'personalCodeFormat']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'personal_code' => 'Personal Code',
            'phone' => 'Phone',
            'active' => 'Active',
            'dead' => 'Dead',
            'lang' => 'Lang',
        ];
    }

    /**
     * @return Datetime user birthday
     */
    public function getBirthDate()
    {
        $year = substr($this->personal_code, 1, 2);
        $month = substr($this->personal_code, 3, 2);
        $day = substr($this->personal_code, 5, 2);
        $birthDate = new Datetime($year . '-' . $month . '-' . $day);
        return $birthDate;
    }

    /**
     * @return bool|\DateInterval user age
     */
    public function age()
    {
        $now = new DateTime();
        return $this->getBirthDate()->diff($now);
    }

    /**
     * @param $attribute
     */
    public function personalCodeLength($attribute)
    {
        if (strlen($this->$attribute) !== 11) {
            $this->addError($attribute, 'The personal code must be 11 digits long.');
        }
    }

    public function personalCodeFormat($attribute)
    {
        $regex = '/^[1-6][0-9]{2}[0-1][0-9][0-9]{2}[0-9]{4}$/';
        if (!preg_match($regex, $this->$attribute)) {
            $this->addError($attribute, 'The personal code is invalid.');
        }
    }


}
