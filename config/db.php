<?php
use \codemix\yii2confload\Config;
return [
    'class' => \yii\db\Connection::class,
    'dsn' => 'pgsql:host=' . Config::env("POSTGRES_HOST", 'db') . ';dbname=' . Config::env('POSTGRES_DB','example'),
    'username' => Config::env('POSTGRES_USER', 'example'),
    'password' => Config::env('POSTGRES_PASSWORD', 'example'),
    'charset' => 'utf8'
];
