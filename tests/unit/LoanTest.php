<?php

use \app\models\User;
use \app\models\Loan;

/**
 * Class LoanTest
 */
class LoanTest extends \Codeception\Test\Unit
{

    // tests
    /**
     * Test user age
     */
    public function testUserAge()
    {
        $user = User::findOne(7518);
        self::assertEquals(40, $user->age()->y);
    }

    /**
     * Test is underAge
     */
    public function testUnderAge()
    {
        $loan = Loan::findOne(36470);

        self::assertFalse($loan->isUnderage());

    }
}